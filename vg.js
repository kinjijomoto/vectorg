(function () {
    'use strict';

    function findPos(obj) {
        var curleft = 0,
            curtop = 0;
        if (obj.offsetParent) {
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
            return [curleft, curtop];
        } else {
            return false;
        }
    }

    function getCoords(event) {
        event = event || window.event;
        if (event.pageX || event.pageY) {
            return {
                x: event.pageX,
                y: event.pageY
            };
        }
        return {
            x: event.clientX + document.body.scrollLeft - document.body.clientLeft,
            y: event.clientY + document.body.scrollTop - document.body.clientTop
        };
    }


    function DrwRect(event) {
        var mousePos = getCoords(event);
        var currentX = mousePos.x - offset[0];
        var currentY = mousePos.y - offset[1];
        var width = currentX - startX;
        var height = currentY - startY;
        if (rect) {
            if (width < 0) {
                rect.attr({
                    'x': currentX,
                        'width': width * -1
                });
            } else {
                rect.attr({
                    'x': startX,
                        'width': width
                });
            }
            if (height < 0) {
                rect.attr({
                    'y': currentY,
                        'height': height * -1
                });
            } else {
                rect.attr({
                    'y': startY,
                        'height': height
                });
            }
        }
        rect.node.setAttribute("id", "rect");
    }

    function DrwCirc(event) {
        var mousePos = getCoords(event);
        var currentX = mousePos.x - offset[0];
        var currentY = mousePos.y - offset[1];
        var rad = currentX - startX;
        circ.attr({
            'x': currentX,
                'y': currentY,
                'r': rad,
                'fill': 'none'
        });
        circ.node.setAttribute("id", "circle");
    }

    function DrwDot(event) {
        var mousePos = getCoords(event);
        var currentX = mousePos.x - offset[0];
        var currentY = mousePos.y - offset[1];
        circ.attr({
            'x': currentX,
                'y': currentY,
                'r': 3,
                'fill': 'black'
        });
        circ.node.setAttribute("id", "dot");
    }
    
    function DrwText(event) {
        var mousePos = getCoords(event);
        var currentX = mousePos.x - offset[0];
        var currentY = mousePos.y - offset[1];
        var text = paper.text(0, 0, " ").attr({
            'font-size': '15px'
        }).transform(['T', currentX, currentY]);
        paper.inlineTextEditing(text);
        text.hover(function () {
            var input = this.inlineTextEditing.startEditing();
            input.addEventListener("blur", function (e) {
                text.inlineTextEditing.stopEditing();
            }, true);
        });
        text.node.setAttribute("id", "text");
    }

    function DrwLine(event) {
        var mousePos = getCoords(event);
        var currentX = mousePos.x - offset[0];
        var currentY = mousePos.y - offset[1];
        line.animate({
            path: [
                ["M", startX, startY],
                ["L", currentX, currentY]
            ]
        }, 1);
        line.node.setAttribute("id", "line");
    }


    var div_paper = document.getElementById('paper');
    var paper = new Raphael('paper');
    var rect;
    var circ;
    var txt;
    var line;
    var startX = 0,
        startY = 0;
    var offset = findPos(div_paper);
    var i = 0;

    div_paper.onmousedown = function (event) {
        var mouseCoords = getCoords(event);
        startX = mouseCoords.x - offset[0];
        startY = mouseCoords.y - offset[1];
        switch (i) {
            case 1:
                rect = paper.rect(startX, startY, 0, 0);
                document.onmousemove = DrwRect;
                break
            case 2:
                circ = paper.circle(startX, startY, 0);
                document.onmousemove = DrwCirc;
                break
            case 3:
                line = paper.path("M" + 0 + 0 + "L" + 0 + 0);;
                document.onmousemove = DrwLine;
                break
            case 4:
                circ = paper.circle(startX, startY, 0);
                document.onmousedown = DrwDot;
                break
            case 5:
                document.onmousedown = DrwText;
                break
            default:
                i = 0;
        }
    };

    document.onmouseup = function (event) {
        document.onmousemove = null;
        document.onmousedown = null;
    };

    jQuery(':button').click(function () {
        if (this.id == 'rect') {
            i = 1;
        } else if (this.id == 'circ') {
            i = 2;
        } else if (this.id == 'line') {
            i = 3;
        } else if (this.id == 'dot') {
            i = 4;
        } else if (this.id == 'txt') {
            i = 5;
        }else if (this.id == 'clear') {
            paper.clear();
        }

    });
})();